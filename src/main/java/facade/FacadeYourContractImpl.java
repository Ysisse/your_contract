package facade;

import exception.UserIntrouvableException;
import modele.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FacadeYourContractImpl implements FacadeYourContract {

    private List<Contrat> contratList;
    private List<User> userList;
    private User userActuel;
    public List<TypeCategorie> typeCategorieList;
    public int compteurContrat;


    @PostConstruct
    public void init() {
        userList = new ArrayList<>();
        typeCategorieList = new ArrayList<>();
        contratList = new ArrayList<>();

        TypeCategorie typeCategorie = new TypeCategorie(0L, "electronique",20);
        TypeCategorie typeCategorie2 = new TypeCategorie(1L, "voiture",40);
        typeCategorieList.add(typeCategorie);
        typeCategorieList.add(typeCategorie2);



        Client client1 = new Client(0L, "billy", "billy");
        Client client2 = new Client(2L, "jean", "jean");
        Conseiller conseiller1 = new Conseiller(1L, "conseiller", "conseiller");

        Contrat contrat1 = new Contrat(2L, "trotinette", LocalDate.now(), typeCategorie, "", client1,typeCategorie.getPrix());
        Contrat contrat2 = new Contrat(0L,"iphone13", LocalDate.now(),typeCategorie,"", client2,typeCategorie.getPrix());
        Contrat contrat3 = new Contrat(1L,"dualtronx", LocalDate.now(),typeCategorie,"", client2,typeCategorie.getPrix());
        Contrat contratV = new Contrat(3L,"Nissan", LocalDate.now(),typeCategorie2,"C'est une très belle voiture, elle est chère donc je me fiche du prix", client1,typeCategorie2.getPrix());
        Contrat contratY = new Contrat(4L,"Mini", LocalDate.now(),typeCategorie2,"C'est une petite voiture", client2,typeCategorie2.getPrix());

        contratV.validation();
        contratY.validation();
        conseiller1.addClient(client1);
        conseiller1.addClient(client2);
        userList.add(client1);
        userList.add(client2);
        userList.add(conseiller1);

        client1.addContrat(contrat1);
        client2.addContrat(contrat2);
        client2.addContrat(contrat3);
        client1.addContrat(contratV);
        client2.addContrat(contratY);
        contratList.add(contrat1);
        contratList.add(contrat2);
        contratList.add(contrat3);
        contratList.add(contratV);
        contratList.add(contratY);

        compteurContrat = contratList.size();
    }

    @Override
    public User getUser() {
        return userActuel;
    }

    @Override
    public Client getClient(long idClient) throws UserIntrouvableException {
        return (Client) userList.stream()
                .filter(u -> u.getId().equals(idClient) && u instanceof Client)
                .findFirst()
                .orElseThrow(UserIntrouvableException::new);
    }

    @Override
    public Conseiller getConseiller(long idConseiller) throws UserIntrouvableException {
        return (Conseiller) userList.stream()
                .filter(u -> u.getId().equals(idConseiller) && u instanceof Conseiller)
                .findFirst()
                .orElseThrow(UserIntrouvableException::new);
    }

    @Override
    public User seConnecter(String pseudo, String mdp) throws UserIntrouvableException {

        userActuel = userList.stream()
                .filter(u -> u.getPseudo().equals(pseudo) && u.getPassword().equals(mdp))
                .findFirst()
                .orElseThrow(UserIntrouvableException::new);
        return userActuel;
    }

    @Override
    public void seDeconnecter(Long idUser) throws UserIntrouvableException {
        userActuel = null;
    }

    @Override
    public void creerContrat(Contrat contrat, Long idClient) throws UserIntrouvableException {
        Client client = getClient(idClient);
        getClient(idClient).getContratList().add(contrat);
        this.contratList.add(contrat);
    }

    @Override
    public void deleteContrat(Long idContrat, Long idClient) throws UserIntrouvableException {
        getClient(idClient).getContratList().removeIf(c -> c.getIdContrat().equals(idContrat));
    }

    @Override
    public void valideContrat(Long idContrat, Long idConseiller) throws UserIntrouvableException {
        listeContratAValide(idConseiller)
                .stream()
                .filter(c -> c.getIdContrat().equals(idContrat))
                .forEach(c -> c.validation());
    }

    @Override
    public void refusContrat(Long idContrat, Long idConseiller, String raisonRefus) throws UserIntrouvableException {
        listeContratAValide(idConseiller)
                .stream()
                .filter(c -> c.getIdContrat().equals(idContrat))
                .forEach(c -> c.setRaisonRefus(raisonRefus));
    }

    @Override
    public void modifieContrat(Contrat contrat, Long idClient) throws UserIntrouvableException {
        getClient(idClient).getContratList()
                .stream()
                .filter(c -> c.getIdContrat().equals(contrat.getIdContrat()))
                .forEach(c -> c = contrat);
    }

    @Override
    public List<Contrat> listeContratAValide(Long idConseiller) throws UserIntrouvableException {
        List<Contrat> contratList = new ArrayList<>();

        for (Client client : getConseiller(idConseiller).getListClient()
        ) {
            for (Contrat contrat : client.getContratList()
            ) {
                if (!contrat.getValide() && Objects.isNull(contrat.getRaisonRefus())) {
                    contratList.add(contrat);
                }
            }
        }
        return contratList;
    }

    @Override
    public List<Contrat> listeContratValider(Long idClient) throws UserIntrouvableException {
        return getClient(idClient).getContratList()
                .stream()
                .filter(contrat -> contrat.getValide())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contrat> listeContratAModifier(Long idClient) throws UserIntrouvableException {
        return getClient(idClient).getContratList()
                .stream()
                .filter(contrat -> !contrat.getValide() && (Objects.isNull(contrat.getRaisonRefus()) || Objects.equals("", contrat.getRaisonRefus())))
                .collect(Collectors.toList());
    }

    @Override
    public Boolean verifContrat(Contrat contrat) {
        Boolean good = false;
        return good;
    }

    @Override
    public User userById(Long idUser) {
        return userList.stream().filter(u -> u.getId().equals(idUser)).findFirst().get();
    }

    @Override
    public List<Contrat> listeContratReffuseClient(Long idClient) throws UserIntrouvableException {
        List<Contrat> contratList = new ArrayList<>();
        for (Contrat contrat : getClient(idClient).getContratList()) {
            if (!Objects.isNull(contrat.getRaisonRefus())) {
                contratList.add(contrat);
            }
        }
        return contratList;
    }

    @Override
    public Contrat detailContrat(Long idContrat){
        for (Contrat contrat: contratList){
            if (contrat.getIdContrat()==idContrat){
                return contrat;
            }
        }
        return contratList.get(0);
    }


}
