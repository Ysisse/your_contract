package facade;

import exception.UserIntrouvableException;
import modele.Client;
import modele.Conseiller;
import modele.Contrat;
import modele.User;

import java.util.List;

public interface FacadeYourContract {

    User getUser();

    public Client getClient(long idClient) throws UserIntrouvableException;

    public Conseiller getConseiller(long idConseiller) throws UserIntrouvableException;

    public User seConnecter(String pseudo, String mdp) throws UserIntrouvableException;

    public void seDeconnecter(Long idUser) throws UserIntrouvableException;

    public void creerContrat(Contrat contrat, Long idClient) throws UserIntrouvableException;

    public void deleteContrat(Long idContrat, Long idClient) throws UserIntrouvableException;

    public void valideContrat(Long idContrat, Long idConseiller) throws UserIntrouvableException;

    public void refusContrat(Long idContrat, Long idConseiller, String raisonRefus) throws UserIntrouvableException;

    public void modifieContrat(Contrat contrat, Long idClient) throws UserIntrouvableException;

    public List<Contrat> listeContratAValide(Long idConseiller) throws UserIntrouvableException;

    public List<Contrat> listeContratValider(Long idClient) throws UserIntrouvableException;

    public List<Contrat> listeContratAModifier(Long idClient) throws UserIntrouvableException;

    public List<Contrat> listeContratReffuseClient(Long idClient) throws UserIntrouvableException;

    public Boolean verifContrat(Contrat contrat);

    public User userById(Long idUser);

    public Contrat detailContrat(Long idContrat);
}