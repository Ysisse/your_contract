package modele;

import java.util.ArrayList;
import java.util.List;

public class Conseiller extends User{
    private List<Client> listClient;

    public Conseiller(Long id, String pseudo, String password) {
        super(id, pseudo, password);
        listClient = new ArrayList<>();
    }

    public Conseiller(Long id, String pseudo, String password, List<Client> listClient) {
        super(id, pseudo, password);
        this.listClient = listClient;
    }

    public List<Client> getListClient() {
        return listClient;
    }

    public void addClient(Client client){
        this.listClient.add(client);
    }
}
