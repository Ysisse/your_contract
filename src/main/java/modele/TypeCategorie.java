package modele;

public class TypeCategorie {
    private Long idTypeCategorie;
    private String nomCategorie;
    private double prix;

    public TypeCategorie(Long idTypeCategorie, String nomCategorie, double prix) {
        this.idTypeCategorie = idTypeCategorie;
        this.nomCategorie = nomCategorie;
        this.prix =prix;
    }

    public Long getIdTypeCategorie() {
        return idTypeCategorie;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
}
