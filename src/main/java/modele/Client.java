package modele;

import java.util.ArrayList;
import java.util.List;

public class Client extends User{
    private List<Contrat> contratList;

    public Client(Long id, String pseudo, String password) {
        super(id, pseudo, password);
        contratList = new ArrayList<>();
    }

    public List<Contrat> getContratList() {
        return contratList;
    }

    public void addContrat(Contrat contrat){
        contratList.add(contrat);
    }

    public void deleteContrat(Long idContrat){
        contratList.removeIf(c -> c.getIdContrat().equals(idContrat));
    }
}
