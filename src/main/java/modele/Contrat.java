package modele;

import java.time.LocalDate;

public class Contrat {
    private Long idContrat;
    private String appareil;
    private TypeCategorie typeCategorie;
    private LocalDate dateCreation;
    private Boolean valide;
    private String raisonRefus;
    private String description;
    private Double prix;
    private Client client;
    //TODO les photos


    public Contrat(Long idContrat, String appareil, LocalDate dateCreation, TypeCategorie typeCategorie, String description, Client client, double prix) {
        this.idContrat = idContrat;
        this.appareil = appareil;
        this.dateCreation = dateCreation;
        this.valide = false;
        this.typeCategorie=typeCategorie;
        this.raisonRefus=null;
        this.description=description;
        this.client = client;
        this.prix=prix;
    }

    public Long getIdContrat() {
        return idContrat;
    }

    public String getAppareil() {
        return appareil;
    }

    public void setAppareil(String appareil) {
        this.appareil = appareil;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setIdContrat(Long idContrat) {
        this.idContrat = idContrat;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public String getRaisonRefus() {
        return raisonRefus;
    }

    public void refus(String raison){
        this.raisonRefus = raison;
    }
    public TypeCategorie getTypeCategorie() {
        return typeCategorie;
    }

    public void setTypeCategorie(TypeCategorie typeCategorie) {
        this.typeCategorie = typeCategorie;
    }

    public void setRaisonRefus(String raisonRefus) {
        this.raisonRefus = raisonRefus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void validation(){
        this.valide = true;
        this.raisonRefus = null;
        this.dateCreation = LocalDate.now();
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Contrat{" +
                "idContrat='" + idContrat + '\'' +
                ", appareil=" + appareil +
                ", dateCreation=" + dateCreation +
                ", valide=" + valide +
                ", raisonRefus='" + raisonRefus + '\'' +
                '}';
    }
}
