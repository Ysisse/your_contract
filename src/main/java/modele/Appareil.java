package modele;

public class Appareil {
    private Long id;
    private Categorie categorie;
    private String nomAppareil;
    private Long prixActuel;

    public Appareil(Long id, Categorie categorie, String nomAppareil, Long prixActuel) {
        this.id = id;
        this.categorie = categorie;
        this.nomAppareil = nomAppareil;
        this.prixActuel = prixActuel;
    }

    public Long getId() {
        return id;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getNomAppareil() {
        return nomAppareil;
    }

    public void setNomAppareil(String nomAppareil) {
        this.nomAppareil = nomAppareil;
    }

    public Long getPrixActuel() {
        return prixActuel;
    }

    public void setPrixActuel(Long prixActuel) {
        this.prixActuel = prixActuel;
    }
}
