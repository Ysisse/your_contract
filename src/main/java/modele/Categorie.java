package modele;

public class Categorie {
    private Long idCategorie;
    private String nomCategorie;
    private TypeCategorie typeCategorie;

    public Categorie(Long idCategorie, String nomCategorie, TypeCategorie typeCategorie) {
        this.idCategorie = idCategorie;
        this.nomCategorie = nomCategorie;
        this.typeCategorie = typeCategorie;
    }

    public Long getIdCategorie() {
        return idCategorie;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public TypeCategorie getTypeCategorie() {
        return typeCategorie;
    }

    public void setTypeCategorie(TypeCategorie typeCategorie) {
        this.typeCategorie = typeCategorie;
    }
}
