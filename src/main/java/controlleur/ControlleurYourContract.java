package controlleur;

import exception.UserIntrouvableException;
import facade.FacadeYourContract;
import facade.FacadeYourContractImpl;
import modele.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import java.time.LocalDate;

@Controller
@SessionAttributes("courant")
@RequestMapping( "/")
public class ControlleurYourContract {

    @Autowired
    FacadeYourContractImpl facadeYourContract;

    @GetMapping("/")
    public String pageLogin(Model model){
        model.addAttribute(new User());
        model.addAttribute("courant",null);
        return "login";
    }

    @PostMapping(value = "/login")
    public String seConnecter(User user, BindingResult result, Model model){
        try {
            user = this.facadeYourContract.seConnecter(user.getPseudo(), user.getPassword());
            model.addAttribute("courant",user);
            //model.addAttribute("idCourant",user.getId());
            if(user instanceof Client) {
                model.addAttribute("listContrat", facadeYourContract.listeContratValider(user.getId()));
                model.addAttribute("listTypeCategorie", facadeYourContract.typeCategorieList);
                model.addAttribute("contrats", facadeYourContract.listeContratReffuseClient(user.getId()));
                return "accueilClient";
            }
            else{
                model.addAttribute("listContrat",facadeYourContract.listeContratAValide(user.getId()));
                return "accueilConseille";
            }
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
            return "login";
        }
    }

    @GetMapping(value = "/souscrire")
    public String souscrire(Model model){
        model.addAttribute("listTypeCategorie", facadeYourContract.typeCategorieList);
        return "nouveauContrat";
    }
    @GetMapping(value = "/accueil")
    public String retourAccueil(Model model){
        User user = (User) model.getAttribute("courant");
        try {
            user = this.facadeYourContract.seConnecter(user.getPseudo(), user.getPassword());
            model.addAttribute("courant",user);
            //model.addAttribute("idCourant",user.getId());
            if(user instanceof Client) {
                model.addAttribute("listContrat", facadeYourContract.listeContratValider(user.getId()));
                model.addAttribute("listTypeCategorie", facadeYourContract.typeCategorieList);
                model.addAttribute("contrats", facadeYourContract.listeContratReffuseClient(user.getId()));
                return "accueilClient";
            }
            else{
                model.addAttribute("listContrat",facadeYourContract.listeContratAValide(user.getId()));
                return "accueilConseille";
            }
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
            return "login";
        }
    }

    @GetMapping(value = "/deconexion")
    public String seDeconnecter(Model model){
        try {
            model.addAttribute(new User());
            this.facadeYourContract.seDeconnecter((Long) model.getAttribute("idCourant"));
            model.addAttribute("idCourant",null);
            return "login";
        } catch (UserIntrouvableException e) {
            return "login";
        }
    }

    @PostMapping(value = "/contrat")
    public String creerContrat(Contrat contrat){
        try {
            this.facadeYourContract.creerContrat(contrat, this.facadeYourContract.getUser().getId());
        } catch (UserIntrouvableException e) {
            return "login";
        }
        User user = facadeYourContract.getUser();
        if(user instanceof Client)
            return "accueilClient";
        else
            return "accueilConseille";
    }

    @DeleteMapping(value = "/contrat/{idContrat}")
    public String deleteContrat(@PathVariable Long idContrat){
        try {
            this.facadeYourContract.deleteContrat(idContrat, this.facadeYourContract.getUser().getId());
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
            return "login";
        }
        return "accueilClient";
    }

    @GetMapping(value = "/contrat/valide/{idContrat}")
    public String valideContrat(@PathVariable Long idContrat, Model model){
        try {
            this.facadeYourContract.valideContrat(idContrat, this.facadeYourContract.getUser().getId());
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
            return "login";
        }
        try {
            model.addAttribute("listContrat",facadeYourContract.listeContratAValide(facadeYourContract.getUser().getId()));
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
        }
        return "accueilConseille";
    }

    @PostMapping(value = "/contrat/refus/{idContrat}")
    public String refusContrat(@PathVariable Long idContrat, String raisonRefus, Model model){
        try {
            this.facadeYourContract.refusContrat(idContrat, this.facadeYourContract.getUser().getId(), raisonRefus);
        } catch (UserIntrouvableException e) {
            return "login";
        }
        try {
            model.addAttribute("listContrat",facadeYourContract.listeContratAValide(facadeYourContract.getUser().getId()));
        } catch (UserIntrouvableException e) {
            e.printStackTrace();
        }
        return "accueilConseille";
    }

    @PostMapping(value = "/contrat/modif")
    public String modifieContrat(Contrat contrat){
        try {
            this.facadeYourContract.modifieContrat(contrat, this.facadeYourContract.getUser().getId());
        } catch (UserIntrouvableException e) {
            return "login";
        }
        return "accueilClient";
    }

    @GetMapping(value = "/contrat/detail/{idContrat}")
    public String detailContrat(@PathVariable Long idContrat, Model model){
        Contrat contrat = this.facadeYourContract.detailContrat(idContrat);
        model.addAttribute("contrat",contrat);
        return "detailContrat";
    }

    @GetMapping(value="/contrat/listeContratRefuse")
    public String listeContratRefuse(Model model){
        try{
            model.addAttribute("listContrat",this.facadeYourContract.listeContratReffuseClient(this.facadeYourContract.getUser().getId()));
        } catch (UserIntrouvableException e) {
            return "login";
        }
        return "ListeContratsRefuse";
    }

    @GetMapping(value = "/contrat/detailClient/{idContrat}")
    public String detailContratClient(@PathVariable Long idContrat, Model model){
        Contrat contrat = this.facadeYourContract.detailContrat(idContrat);
        model.addAttribute("contrat",contrat);
        return "detail";
    }

    @PostMapping(value = "/contrat/description")
    public String afficheContrat(Long idContrat,Model model){
        Contrat contrat = ((Client)model.getAttribute("courant"))
                .getContratList()
                .stream()
                .filter(c -> c.getIdContrat().equals(idContrat))
                .findFirst()
                .get();
        model.addAttribute("contrat", contrat);
        return "detail";
    }

    @PostMapping(value = "/souscrire")
    public String souscrireContrat(Model model, String description, String appareil, long leType){

        try {
            TypeCategorie type=facadeYourContract.typeCategorieList.stream().filter(t -> t.getIdTypeCategorie().equals(leType)).findFirst().get();
            Contrat contrat = new Contrat(
                    (long) facadeYourContract.compteurContrat,
                    appareil,
                    LocalDate.now(),
                    type,
                    description,
                    (Client) model.getAttribute("courant"),
                    type.getPrix());
            facadeYourContract.creerContrat(contrat,((User)model.getAttribute("courant")).getId());
            facadeYourContract.compteurContrat++;
            model.addAttribute("listContrat", facadeYourContract.listeContratValider(((Client) model.getAttribute("courant")).getId()));
            return "accueilClient";
        } catch (UserIntrouvableException e) {
            return "login";
        }


    }
}
